// ======================================
// Config Files (config-apps.json)
// ======================================
// - ['GROUP_ID']['apps']['APPLICATION_ID']['ports'] = 8090:80
private def _loadConfig(String configFileId, String targetLoc) {
    def config
    configFileProvider([configFile(fileId: "${configFileId}", targetLocation: "${targetLoc}")]) {
        config = readJSON file: "${targetLoc}"
    }
    return config
}

def restoreAndBuildNet7() {
    def cfgApps = _loadConfig('config-apps', 'cfgApps')
    def app = cfgApps['apps']["${APPLICATION_ID}"]
    dir("${app.solution}") {
        dotnet.dnRestore()
        dotnet.dnBuild()
    }
}

def test() {
    def cfgApps = _loadConfig('config-apps', 'cfgApps')
    def app = cfgApps['apps']["${APPLICATION_ID}"]
    dotnet.dnTest(app.tests)
}

def clean() {
    def cfgApps = _loadConfig('config-apps', 'cfgApps')
    def app = cfgApps['apps']["${APPLICATION_ID}"]
    dir("${app.distribution}") {
        sh 'rm -rf ./tempPublish'
    }
}

def deployNet7Angular() {
    def cfgApps = _loadConfig('config-apps', 'cfgApps')
    def app = cfgApps['apps']["${APPLICATION_ID}"]
    def cfgData = _loadConfig('config-env', 'cfgData')
    def iisCredentials = cfgData.iis_credentials
    dir("${app.distribution}") {
        dotnet.dnPublish()
    }
    dir("${app.distribution}/tempPublish") {
        withCredentials([usernamePassword(credentialsId: "${iisCredentials}", passwordVariable: 'SMB_PASS', usernameVariable: 'SMB_USER')]) {

            smb.validFolderSite(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                app.folderSite
                            )
            smb.setSiteOffline(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                app.folderSite
                            )

            smb.deleteFiles(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                "${app.folderSite}/wwwroot"
                            )

            smb.copyFilesToServer(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                app.folderSite
                            )

            smb.setSiteOnline(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                app.folderSite
                            )
        }
    }
}

def deployNet() {
    def cfgApps = _loadConfig('config-apps', 'cfgApps')
    def app = cfgApps['apps']["${APPLICATION_ID}"]
    def cfgData = _loadConfig('config-env', 'cfgData')
    def iisCredentials = cfgData.iis_credentials
    dir("${app.distribution}") {
        dotnet.dnPublish()
    }
    dir("${app.distribution}/tempPublish") {
        withCredentials([usernamePassword(credentialsId: "${iisCredentials}", passwordVariable: 'SMB_PASS', usernameVariable: 'SMB_USER')]) {

            smb.validFolderSite(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                app.folderSite
                            )

            smb.setSiteOffline(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                app.folderSite
                            )
            sleep(10)

            smb.copyFilesToServer(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                app.folderSite
                            )

            smb.setSiteOnline(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                app.folderSite
                            )
        }
    }
}


def deployAngular() {
    def cfgApps = _loadConfig('config-apps', 'cfgApps')
    def app = cfgApps['apps']["${APPLICATION_ID}"]
    def cfgData = _loadConfig('config-env', 'cfgData')
    def iisCredentials = cfgData.iis_credentials

    dir("${app.distribution}") {
        withCredentials([usernamePassword(credentialsId: "${iisCredentials}", passwordVariable: 'SMB_PASS', usernameVariable: 'SMB_USER')]) {

            smb.validFolderSite(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                app.folderSite
                            )
                            
            smb.setSiteOffline(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                app.folderSite
                            )
              smb.deleteFiles(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                "${app.folderSite}"
              )

            smb.copyFilesToServer(
                                env[app.server],
                                env.SMB_USER,
                                env.SMB_PASS,
                                app.folderSite
                            )
        }
    }
}