private def _loadConfig(String configFileId, String targetLoc) {
    def config
    configFileProvider([configFile(fileId: "${configFileId}", targetLocation: "${targetLoc}")]) {
        config = readJSON file: "${targetLoc}"
    }
    return config
}

void sendSuccess(Map params) {
    slackSend color: 'good', channel: "${params.SlackChannel}", message: "${params.Message}"
}

void sendError(Map params) {
    slackSend failOnError:true, color: 'danger', channel: "${params.SlackChannel}", message: "${params.Message}"
}

void sendInfo(Map params) {
    slackSend  color: 'info', channel: "${params.SlackChannel}", message: "${params.Message}"
}

void sendWarning(Map params) {
    slackSend color: 'warning', channel: "${params.SlackChannel}", message: "${params.Message}"
}

void sendIniPipeline() {
    def cfgData = _loadConfig('config-env', 'cfgData')
    def channelSlack = cfgData.channel_slack
    def env_branch = cfgData.env_branch
    if (env_branch == 'qa' || env_branch == 'prod') {
        sendWarning(
        SlackChannel: channelSlack,
         Message: "Iniciando Despliegue :rocket: - Job: *${JOB_NAME}* - Build # *${BUILD_NUMBER}* - versión: *${VERSION_APP}* "
        )
    }else {
        sendWarning(
        SlackChannel: channelSlack,
       Message: "Iniciando Despliegue :rocket: - Job: *${JOB_NAME}* - Build # *${BUILD_NUMBER}* "
        )
    }
}

void sendFailurePipeline() {
    def cfgData = _loadConfig('config-env', 'cfgData')
    def channelSlack = cfgData.channel_slack
    sendError(
        SlackChannel: channelSlack,
        Message: "Despliegue KO :face_palm: - Job: *${JOB_NAME}* - Build # *${BUILD_NUMBER}* (<${BUILD_URL}|Open>)"
        )
}

void sendSuccessPipeline() {
    def cfgData = _loadConfig('config-env', 'cfgData')
    def channelSlack = cfgData.channel_slack
    def cfgApps = _loadConfig('config-apps', 'cfgApps')
    def app = cfgApps['apps']["${APPLICATION_ID}"]
    sendSuccess(
        SlackChannel: channelSlack,
        Message: "Despliegue OK :beer: - Job: *${JOB_NAME}* - Build # *${BUILD_NUMBER}*\n <${app.site}>"
        )
}

