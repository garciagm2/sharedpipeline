def call(Map pipelineParams) {
    pipeline {
        agent { label 'linux' }
        environment {
            APPLICATION_ID = "${pipelineParams.IdApp}"
        }
        tools {
            nodejs "${pipelineParams.NodeVersion}"
        }
        stages {
            stage('Inicio del Pipeline') {
                steps {
                    script {
                        slack.sendIniPipeline()
                    }
                }
            }
            stage('Restore') {
                steps {
                    // Restaurar las dependencias del proyecto.
                    sh 'npm install'
                }
            }
            stage('Build') {
                steps {
                    script{
                        ng_ceplan.build()
                    }
                }
            }
            stage('Deploy') {
                steps {
                    script {
                        iis_ceplan.deployAngular()
                    }
                }
            }
        }
        post {
            always { cleanWs() }
            success {
                script {
                    slack.sendSuccessPipeline()
                }
            }
            failure {
                script {
                   slack.sendFailurePipeline()
                }
            }
        }
    }
}
