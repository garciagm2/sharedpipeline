def call(Map pipelineParams) {
    pipeline {
        agent { label 'linux' }
        environment {
            APPLICATION_ID = "${pipelineParams.IdApp}"
        }
        stages {
            stage('Inicio del Pipeline') {
                steps {
                    script {
                        slack.sendIniPipeline()
                    }
                }
            }
            stage('cloneEnv') {
                steps {
                    script {
                        git_ceplan.cloneEnv()
                        git_ceplan.copyAppSettings()
                    }
                }
            }
            stage('Restore and Build') {
                steps {
                    script {
                        iis_ceplan.restoreAndBuildNet7()
                    }
                }
            }
            stage('Test') {
                steps {
                    script {
                        iis_ceplan.test()
                    }
                }
            }
            stage('Deploy') {
                steps {
                    script {
                        iis_ceplan.deployNet()
                    }
                }
            }
            stage('Cleanup') {
                steps {
                    script {
                        iis_ceplan.clean()
                    }
                }
            }
        }
        post {
            always { cleanWs() }
            success {
                script {
                    slack.sendSuccessPipeline()
                }
            }
            failure {
                script {
                    slack.sendFailurePipeline()
                }
            }
        }
    }
}
