private def _loadConfig(String configFileId, String targetLoc) {
    def config
    configFileProvider([configFile(fileId: "${configFileId}", targetLocation: "${targetLoc}")]) {
        config = readJSON file: "${targetLoc}"
    }
    return config
}
def build() {
    def cfgData = _loadConfig('config-env', 'cfgData')
    def envBranch = cfgData.env_branch
    def buildConfig = ''

    switch (envBranch) {
        case 'dev':
            buildConfig = 'development'
            break
        case 'qa':
            buildConfig = 'certification'
            break
        case 'prod':
            buildConfig = 'production'
            break
        default:
            error("Branch not recognized: ${envBranch}")
    }

    sh "npx ng build --configuration=${buildConfig}"
}