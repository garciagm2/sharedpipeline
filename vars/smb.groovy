void ejecutarSmbClient(String server, String user, String pwd, String directory, String smbCommand) {
    if (server == null || user == null || pwd == null || directory == null ||
        server.trim() == '' || user.trim() == '' || pwd.trim() == '' || directory.trim() == '') {
        println 'Parámetros inválidos para smbclient'
        return
        }

    sh """
    smbclient ${server} -U ${user}%${pwd} -c "cd ${directory}; ${smbCommand}"
    """
}

void validFolderSite(String server, String user, String pwd, String directory) {
    if (server == null || user == null || pwd == null || directory == null ||
        server.trim() == '' || user.trim() == '' || pwd.trim() == '' || directory.trim() == '') {
        Error('Parámetros inválidos para smbclient')
        return false
    }

    try {
        sh script: """
            smbclient ${server} -U ${user}%${pwd} -c "cd ${directory};"
        """, returnStdout: true
        return true
    } catch (Exception e) {
        Error("Error al ejecutar smbclient: ${e.message}")
        return false
    }
}

void setSiteOffline(String server, String user, String pwd, String directory) {
    ejecutarSmbClient(server, user, pwd, directory, 'put /home/jenkins/App_Offline.htm App_Offline.htm')
}

void setSiteOnline(String server, String user, String pwd, String directory) {
    ejecutarSmbClient(server, user, pwd, directory, 'del App_Offline.htm')
}

void copyFilesToServer(String server, String user, String pwd, String directory) {
    ejecutarSmbClient(server, user, pwd, directory, 'recurse ON; prompt OFF; mput *')
}
void deleteFiles(String server, String user, String pwd, String directory) {
    if (directory == null || directory == '') {
        return
    }
    sh """
                        smbclient ${server} -U ${user}%${pwd} <<EOF
                        cd ${directory}
                        prompt OFF
                        recurse ON
                        del *
                        rmdir *
                        exit
                        EOF
                        """
}
