// ==================================
// Config Files (config-data.json)
// ==================================
// - env_repository_url =
// - env_branch =
// - env_credentials =

private def _loadConfig(String configFileId, String targetLoc) {
    def config
    configFileProvider([configFile(fileId: "${configFileId}", targetLocation: "${targetLoc}")]) {
        config = readJSON file: "${targetLoc}"
    }
    return config
}

def cloneEnv() {
    def cfgData = _loadConfig('config-env', 'cfgData')
    def envRepositoryUrl = cfgData.env_repository_url
    def envBranch = cfgData.env_branch
    def envCredentials = cfgData.env_credentials
    if(envBranch!='dev'){
        checkout([
            $class: 'GitSCM',
            branches: [[name: "${envBranch}"]],
            userRemoteConfigs: [[url: "${envRepositoryUrl}", credentialsId: "${envCredentials}"]],
            extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'env']]
        ])
    }
}

def copyAppSettings(){
    def cfgApps = _loadConfig('config-apps', 'cfgApps')
    def app = cfgApps['apps']["${APPLICATION_ID}"]
    def cfgData = _loadConfig('config-env', 'cfgData')
    def envBranch = cfgData.env_branch
    if(envBranch!='dev'){
        sh "cp -r env/${APPLICATION_ID}/* ${app.distribution}/"
    }
}
