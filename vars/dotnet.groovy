void dnRestore() {
    sh 'dotnet restore'
}

void dnBuild() {
    sh 'dotnet build'
}

void dnPublish() {
    sh 'dotnet publish -c Release -o ./tempPublish -v d'
    sh 'ls -la ./tempPublish'
}

void dnTest(List<String> proyectos) {
    if (proyectos == null || proyectos.isEmpty()) {
        println 'No se proporcionaron proyectos para testear.'
        return
    }
    proyectos.each { proyecto ->
            sh "dotnet test ${proyecto} --no-restore --verbosity normal"
    }
}
